/*global require, module, console, app, $, window, _, setTimeout*/
(function () {
    'use strict';

    var menu = function () {
        var menuArray = [
        {
            text: '새로고침',
            disabled: false,
            hidden: false,
            handler: function () {
                app.combo.reloadAll();
            }
        },'-', {
            text: '대시보드',
            disabled: false,
            hidden: app.roleSettings.menu_account(),
            handler: function () {
                app.window.dashboard();
            }
        },'->', {
            text: '로그아웃',
            disabled: false,
            hidden: false,
            handler: function () {
                window.sessionStorage.removeItem('login_info')
                window.location.href = '/cms';
            }
        }];
        return menuArray;
    };

    app.common.menu = menu;

}());