/*global require, module, console, app, window, this, Ext, _*/
(function () {
    'use strict';
    var app = window.app;
    var fbasic = app.common.field_maker.basic;
    var formatter = app.common.formatter;
    var field_set = {
        check: {
            xtype: 'checkcolumn',
            header: '선택',
            dataIndex: 'active',
            width: 60
        },
        rownum: {
            xtype: 'rownumberer',
            width: 48,
            renderer: function (a, b, c) {
                // console.log(a);
                // console.log(b);
                // console.log(c);
                var num = (c.index + 1);
                if (_.isNaN(num)) {
                    num = 0;
                }
                return '<span style="color:grey">' + num + '</span>';
            }
        },
        empty: {
            dataIndex: '',
            header: '',
            flex: 1
        },
        userid: {
            dataIndex: 'userid',
            header: 'USERID',
            hidden: true
        },
        user_id: {
            dataIndex: 'user_id',
            header: '아이디',
            align: 'left'
        },
        uid: {
            dataIndex: 'uid',
            header: 'UID',
            hidden: true,
            width: 30
        },
        account_uid: {
            dataIndex: 'uid',
            header: '계좌번호',
            width: 120
        },
        idx: fbasic({
            dataIndex: 'idx',
            header: '색인',
            align: 'right',
            width: 80
        }),
      
        register_time: fbasic({
            header: '등록시간',
            dataIndex: 'register_time',
            align: 'center',
            xtype: 'numberfield',
            renderer: formatter.time
        }),
  
        withdrawable_amount: fbasic({
            header: '출금가능금액',
            dataIndex: 'withdrawable_amount',
            xtype: 'numberfield',
            renderer: formatter.commify,
            width: 90
        }),
 
        phone: fbasic({
            header: '전화번호',
            dataIndex: 'phone',
            align: 'left',
            width: 100,
            xtype: 'textfield'
        }),
    
        date: fbasic({
            header: '날짜',
            dataIndex: 'date',
            xtype: 'textfield'
        }),
        register_date: fbasic({
            header: '등록일',
            dataIndex: 'register_date',
            xtype: 'numberfield',
            renderer: formatter.dateYYYYMMDD
        }),
        endDate: fbasic({
            header: '마감일',
            dataIndex: 'endDate',
            xtype: 'numberfield',
            renderer: formatter.dateYYYYMMDD
        }),
        createDate: fbasic({
            header: '가입일',
            dataIndex: 'createdAt',
            renderer: formatter.date,
            align : 'center',
            width : 100,
            xtype: 'textfield'
        }),
        editDate: fbasic({
            header: '변경일',
            dataIndex: 'editDate',
            renderer: formatter.date,
            xtype: 'textfield'
        }),
        deleteYN: fbasic({
            header: '삭제 여부',
            dataIndex: 'deleteYN',
            xtype: 'textfield'
        }),
    
        timeDate: fbasic({
            dataIndex: 'time',
            header: '날짜',
            xtype: 'numberfield',
            renderer: formatter.date,
            width: 140
        }),
        // 회원 필드
        memberName : fbasic({
            header : '리셀러명',
            dataIndex : 'username',
            align : 'center',
            width : 110,
            xtype : 'textfield'
        }),
        memberPhone : fbasic({
            header: '전화번호',
            dataIndex: 'phone',
            align: 'center',
            width: 110,
            xtype: 'textfield'
        }),
        point : fbasic({
            header: '포인트',
            dataIndex: 'point',
            align: 'right',
            width: 100,
            xtype: 'textfield',
            text_editable : true,
            renderer : function(data){
                var onlyNum = data.replace(/[^0-9]/g, "");
                var commaNum = onlyNum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return commaNum;
            }
        }),
        unique : {
            dataIndex: 'unique',
            align: 'center',
            hidden: true
        }, 

        //주문필드
        customerName : fbasic({
            header : '고객명',
            dataIndex : 'customer_name',
            align : 'center',
            width : 100,
            xtype : 'textfield'
        }),
        customerPhone : fbasic({
            header: '고객전화번호',
            dataIndex: 'customer_telephone',
            align: 'center',
            width: 110,
            xtype: 'textfield'
        }),
        userName : fbasic({
            header : '리셀러명',
            dataIndex : 'user_name',
            align : 'center',
            width : 100,
            xtype : 'textfield'
        }),
        userPhone : fbasic({
            header: '리셀러전화번호',
            dataIndex: 'user_phone',
            align: 'center',
            width: 110,
            xtype: 'textfield'
        }),
        status : fbasic({
            dataIndex: 'status',
            align: 'center',
            header: '작업상황',
            xtype: 'textfield',
            width: 130,
            combo_editable_order : true
        }), 
        endstatus : fbasic({
            dataIndex: 'status',
            align: 'center',
            header: '작업상황',
            xtype: 'textfield',
            width: 130
        }), 
        orderNum : {
            dataIndex: 'ordernum',
            align: 'center',
            hidden: true
        }, 
        orderAmount :fbasic({
            dataIndex: 'order_amount',
            align: 'right',
            header: '시공금액',
            xtype: 'textfield',
            width: 110,
            text_editable : true
        }),
        orderId : {
            dataIndex: 'order_id',
            align: 'center',
            hidden: true
        }, 
        orderCreateDate :fbasic({
            header: '신청일',
            dataIndex: 'createdAt',
            renderer: formatter.date,
            align : 'center',
            width : 100,
            xtype: 'textfield'
        }),
        place : fbasic({
            header : '시공장소',
            dataIndex : 'place',
            align : 'left',
            width : 300,
            xtype : 'textfield'
        }),

        // 출금
        bank : fbasic({
            header : '출금은행',
            dataIndex : 'bank',
            align : 'center',
            width : 90,
            xtype : 'textfield'
        }),
        accountNum : fbasic({
            header : '출금계좌',
            dataIndex : 'account_num',
            align : 'center',
            width : 120,
            xtype : 'textfield'
        }),
        withdrawPrice : fbasic({
            header : '출금금액',
            dataIndex : 'withdraw_price',
            align : 'center',
            width : 100,
            xtype : 'textfield'
        }),
        accountHolder : fbasic({
            header : '예금주',
            dataIndex : 'account_holder',
            align : 'center',
            width : 90,
            xtype : 'textfield'
        }),
        withdrawStatus : fbasic({
            dataIndex: 'status',
            align: 'center',
            header: '출금상태',
            xtype: 'textfield',
            width: 130,
            combo_editable_withdraw : true
        }), 
        withdrawNum :  {
            dataIndex: 'withdrawnum',
            align: 'center',
            hidden: true
        }, 
        userId :  {
            dataIndex: 'user_id',
            align: 'center',
            hidden: true
        }, 
        withdrawCreateDate :fbasic({
            header: '접수일',
            dataIndex: 'createdAt',
            renderer: formatter.date,
            align : 'center',
            width : 100,
            xtype: 'textfield'
        }),
    };
    app.common.field_set = field_set;
}());