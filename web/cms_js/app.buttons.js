/*global $, require, module, console, app, Ext, console, _, window, moment, async, clearTimeout, setTimeout*/
/*jslint this:true*/

// 새로고침, 단어필터, 이름 전화 번호 검색 버튼 ----> 통합계정관리, 계좌, 정산 세부 필드에 쓰임.
(function () {
    'use strict';

    // 새로고침 버튼 
    app.buttons.reload = {
        xtype: 'button',
        text: '새로고침',
        handler: function () {
            var grid = this.up().up(); // up(): estjs method로 해당 상위 컨테이너로의 이동 매개변수 : select
            var win = grid.up('window');
            var target = app.preset[win.id];
            if (target) { // 새로 추가된게 있을 시 로직
                app.tools.proxyUpdate(target, grid);
            } else { // 데이터 다시 받아와서 배치
                grid.getStore().reload();
            }
        }
    };

    // 단어 필터 인풋
    var typingTimer = null;
    app.buttons.search = function () {
        var form = new Ext.form.TextField({
            emptyText: '단어 필터',
            listeners: {
                change: function () {
                    console.log('change');
                    var that = this;
                    clearTimeout(typingTimer);
                    typingTimer = setTimeout(function () {
                        app.tools.searchAlgo(that);
                    }, 600);
                },
                focus: function () {
                    console.log('focus');
                    var that = this;
                    clearTimeout(typingTimer);
                    typingTimer = setTimeout(function () {
                        app.tools.searchAlgo(that);
                    }, 200);
                }
            }
        });
        return form;
    };

    // 이름 전화 번호 검색을 할 경우 호출 메소드
    var doSearch = function (text, oldStore) {
        var con1 = text.length > 0;
        if (_.isNaN(+text[0]) === false) {
            con1 = text.length > 3;
        }
        if (text && con1) {
            app.MASK.msg = text + ' 를 검색중입니다. 잠시만 기다려 주세요.';
            app.MASK.show();
            $.getJSON('/api/customer_summary?search=' + text, function (res) {
                // console.log(res);
                oldStore.clearData();
                oldStore.loadData(res.items);
                app.MASK.hide();
            });
        } else {
            Ext.Msg.alert('검색 오류', '검색어를 정확하게 입력해 주세요. 전화번호는 3자 이상 하이픈(-) 포함하여 입력해 주세요.');
        }
    };

    // 이름 전화 번호 검색 버튼
    app.buttons.searchServer = function () {
        var form = new Ext.form.TextField({
            emptyText: '이름 전화 번호 검색',
            listeners: {
                specialkey: function (rec, ev) {
                    var that = this;
                    var win = that.up('window');
                    var grid = win.down('grid');
                    var oldStore = grid.getStore();
                    if (ev.getKey() !== ev.ENTER) {
                        return false;
                    }
                    doSearch(rec.lastValue, oldStore);
                }
            }
        });
        return form;
    };

    //엑셀 버튼
    app.buttons.real_excel = function () {
        return {
            text: '',
            iconCls: 'button-excel',
            handler: function () {
                var target = this;
                var store = target.up('window').down('grid').getStore();
                var result = [];
                store.each(function (el) {
                    result.push(el.getData());
                });
                //app.common.field_set_summary().col01().header
                var aaa = app.common.field_set_summary();
                var keyMap = null;
                if (result.length) {
                    keyMap = {};
                    _.each(_.keys(result[0]), function (key) {
                        if (aaa[key]) {
                            var tt = aaa[key]();
                            keyMap[key] = tt.text || tt.header;
                        }
                    });
                }


                _.each(result, function (el) {
                    if (app.combo.partner_map[el.col03]) {
                        el.col03 = app.combo.partner_map[el.col03].name;
                    }
                    if (app.combo.product_map[el.col35]) {
                        el.col35 = app.combo.product_map[el.col35].name;
                    }
                    if (app.combo.security_map[el.col14]) {
                        el.col14 = app.combo.security_map[el.col14].name;
                    }
                });
                // col03 //법인명 app.combo.partners
                // col35 //포트

                if (result.length) {
                    _.each(result, function (el) {
                        _.each(el, function (val, key) {
                            if (keyMap[key]) {
                                el[keyMap[key]] = val;
                                delete el[key];
                            }
                        });
                    });
                    console.log(result);
                    app.MASK.msg = '엑셀 데이터를 추출중입니다.';
                    app.MASK.show();
                    $.post('/jsonToExcel', {
                        title: '통합 계정 관리',
                        list: result
                    }, function (res) {
                        // console.log(res);
                        window.open(res.link);
                        app.MASK.hide();
                    });
                }
            }
        };
    };
}());