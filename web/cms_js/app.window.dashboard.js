/*global require, module, console, app, _*/
(function () {
    'use strict';

    var items = function () {
        var result = [{
            region: 'west',
            layout: 'border',
            flex: 1,
            items: [{
                region: 'center',
                split: true,
                border: false,
                layout: 'fit',
                title: '<span style="color:#44edff">리셀러</span>',
                width: 580,
                items: [
                    app.method.grid_panel(app.preset.member(), true)
                ]
            }]
        }, {
            region: 'center',
            flex: 2,
            layout: 'border',
            split: true,
            items: [{
                region: 'center',
                split: true,
                border: false,
                title: '<span style="color:rgb(90, 255, 97)">주문</span>',
                width: 580,
                flex: 1.5,
                layout: 'fit',
                xtype : 'tabpanel',
                activeTab : 0,
                items: [
                    {
                        title: '진행시공내역',
                        layout: 'fit',
                        border: false,
                        items: [
                            app.method.grid_panel(app.preset.order(), true)
                        ]
                    }, {
                        title: '마감시공내역',
                        layout: 'fit',
                        border: false,
                        items: [
                            app.method.grid_panel(app.preset.end_order(), true)
                        ]
                    }, {
                        title: '취소내역',
                        layout: 'fit',
                        border: false,
                        items: [
                            app.method.grid_panel(app.preset.cancel_order(), true)
                        ]
                    }
                ]
            }, {
                region: 'south',
                split: true,
                border: false,
                title: '<span style="color:rgb(255, 199, 31)">출금</span>',
                flex: 1,
                layout: 'fit',
                xtype : 'tabpanel',
                activeTab : 0,
                items: [
                    {
                        title: '출금내역',
                        layout: 'fit',
                        border: false,
                        items: [
                            app.method.grid_panel(app.preset.withdraw(), true)
                        ]
                    }, {
                        title: '마감출금내역',
                        layout: 'fit',
                        border: false,
                        items: [
                            app.method.grid_panel(app.preset.end_withdraw(), true)
                        ]
                    }
                ]
            }]
        }];
        return result;
    };
    var dashboard = function () {
        app.method.common_panel({
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                id: 'mainToolBar',
                items: app.common.menu()
            }],
            maximized: true,
            border: false,
            id: 'dashboard',
            layout: 'border',
            items: items()
        });
    };
    app.window.dashboard = dashboard;
}());