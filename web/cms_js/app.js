/*global require, module, console, app, window, $, _, Ext*/
(function () {
    'use strict';
    var app = {};
    window.app = app;

    app.window = {};
    app.common = {};
    app.buttons = {};
    app.method = {};
    app.preset = {};
    app.popup = {};
    app.tools = {};
    app.roleSettings = {};
    app.sockets = {
        local: null
    };
    app.edition_color = {
        up: '#ff5722',
        down: '#03a9f4',
        inactiveColor: '#CCCCCC',
        axisColor: '#000000',
        labelTextColor: '#000000'
    };
    app.gridSelected = null;

    app.session = {};

    app.myProductId = $('html').data('userid');
    app.productMap = {};
    app.codeMap = {};
    app.userData = {
        pageSize: 35
    };

    Ext.Date.dayNames = [
        '일',
        '월',
        '화',
        '수',
        '목',
        '금',
        '토'
    ];
    Ext.Date.monthNames = [
        '1월',
        '2월',
        '3월',
        '4월',
        '5월',
        '6월',
        '7월',
        '8월',
        '9월',
        '10월',
        '11월',
        '12월'
    ];
    Ext.Date.monthNumbers = {
        '1월': 1,
        '2월': 2,
        '3월': 3,
        '4월': 4,
        '5월': 5,
        '6월': 6,
        '7월': 7,
        '8월': 8,
        '9월': 9,
        '10월': 10,
        '11월': 11,
        '12월': 12
    };

}());