/*global require, module, console, APP, moment, accounting, app, _, window*/
(function () {
    'use strict';
    var app = window.app;
    var commify = function (n, end) {
        var result = 0;
        var endval = end || 0;
        result = accounting.formatMoney(n, '', endval);
        return result;
    };

    var colorSpan = function (option) {
        var pre = '';
        var num = option.num;
        var p_sign = option.p_sign;
        var m_sign = option.m_sign;
        var n_sign = option.n_sign;
        if (num > 0) {
            pre = '<i style="font-style:normal;color:' + app.edition_color.up + ';">' + p_sign;
        } else if (num < 0) {
            pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';">' + m_sign;
        } else {
            pre = '<i style="font-style:normal;">' + n_sign;
        }
        return pre;
    };

    var formatter;
    formatter = {
        serviceId: function (val) {
            var vals = '' + val;
            var result = '';
            if (app.codeMap[vals]) {
                result = app.codeMap[vals].name;
            } else {
                result = vals;
            }
            return result;
        },
        blog_who: function (str) {
            return str.replace('체결', '<span style="color:yellow">체결</span>');
        },
        yn: function (str) {
            var pre = '';
            if (str === 'Y') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.up + ';">YES</i>';
            } else if (str === 'N') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';">NO</i>';
            } else {
                pre = str;
            }
            return pre;
        },
        on_off: function (str) {
            var pre = '';
            if (str === 'Y') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.up + ';">ON</i>';
            } else if (str === 'N') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';">OFF</i>';
            } else {
                pre = str;
            }
            return pre;
        },
        yes_no: function (str) {
            var pre = '';
            if (str === 'YES') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.up + ';">YES</i>';
            } else if (str === 'NO') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';">NO</i>';
            } else {
                pre = str;
            }
            return pre;
        },
        who: function (str) {
            var val = str.replace(' 체결', '');
            var pre = '';
            if (val === '매수') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.up + ';">매수</i>';
            } else if (val === '매도') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';">매도</i>';
            }
            return pre;
        },
        what: function (str) {
            return (str.split('(')[1]).replace(')', '');
        },
        so: function (str) {
            var val = str.replace('(주문번호 : ', '');
            val = val.replace(')', '');
            return val;
        },
        dateYYYYMMDD: function (n) {
            if (n === 'NA') {
                return '';
            }
            return moment(n).format('YYYY.MM.DD');
        },
        elapsedDays: function (date) {
            var b = moment(date).startOf('day');
            var current = moment();
            return current.diff(b, 'days') + '일';
        },
        toYN: function (val) {
            var pre = 'N';
            if (val === true) {
                pre = 'Y';
            }
            return pre;
        },
        position: function (val) {
            var pre = '관망';
            if (val === 'buy') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.up + ';">매수</i>';
            } else if (val === 'sell') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';">매도</i>';
            }
            return pre;
        },
        auto_type: function (val) {
            var type_map = {
                direct: '고객직접',
                range: '투자전략(범위)'
            };
            return type_map[val] || '';
        },
        positionNum: function (val) {
            var pre = '관망';
            if (val === 2) {
                pre = '<i style="font-style:normal;color:' + app.edition_color.up + ';">[매수]</i>';
            } else if (val === 1) {
                pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';">[매도]</i>';
            }
            return pre;
        },
        feeling: function (val) {
            if (val.length) {
                val = val.replace('상승', '<i style="font-style:normal;color:' + app.edition_color.up + ';">상승</i>');
                val = val.replace('매수', '<i style="font-style:normal;color:' + app.edition_color.up + ';">매수</i>');
                val = val.replace('계약', '<i style="font-style:normal;color:' + app.edition_color.up + ';">계약</i>');
                val = val.replace('통과', '<i style="font-style:normal;color:' + app.edition_color.up + ';">통과</i>');
                val = val.replace('하락', '<i style="font-style:normal;color:' + app.edition_color.down + ';">하락</i>');
                val = val.replace('매도', '<i style="font-style:normal;color:' + app.edition_color.down + ';">매도</i>');
                val = val.replace('배임', '<i style="font-style:normal;color:' + app.edition_color.up + ';">배임</i>');
                val = val.replace('매각', '<i style="font-style:normal;color:' + app.edition_color.up + ';">매각</i>');
            }
            return val;
        },
        diffDate: function (oldDay, currentDay) {
            var b = moment(oldDay);
            var current = moment(currentDay);
            return +current.diff(b, 'days');
        },
        cut2: function (num) {
            return Math.floor(num * 100) / 100;
        },
        cut4: function (num) {
            return Math.floor(num * 1e4) / 1e4;
        },
        toZero: function (str) {
            var result = 0;
            if (str === 'NA' || str === null || _.isNaN(+str)) {
                result = 0;
            } else {
                result = +str;
            }
            return result;
        },
        commifyColor: function (n, deci) {
            var result = '';
            var pre = colorSpan({
                num: n,
                p_sign: '▲&nbsp;',
                m_sign: '▼&nbsp;',
                n_sign: '&nbsp;&nbsp;'
            });
            result = pre + (commify(n, deci)).replace('-', '') + '</i>';
            return result;
        },
        toColor: function (n) {
            var result = '';
            var pre = colorSpan({
                num: n,
                p_sign: '',
                m_sign: '',
                n_sign: ''
            });
            result = pre + (commify(n)) + '</i>';
            return result;
        },
        toColor2: function (n) {
            var result = '';
            var pre = colorSpan({
                num: n,
                p_sign: '+',
                m_sign: '',
                n_sign: ''
            });
            result = pre + (commify(n, 2)) + '</i>';
            return result;
        },
        toColor2Change: function (n, change) {
            var result = '';
            var pre = colorSpan({
                num: change,
                p_sign: '+',
                m_sign: '',
                n_sign: ''
            });
            result = pre + (commify(n, 2)) + '</i>';
            return result;
        },
        toColor200: function (n) {
            var result = '';
            var pre = colorSpan({
                num: n,
                p_sign: '+',
                m_sign: '',
                n_sign: ''
            });
            result = pre + (commify(n * 100, 2)) + '</i>';
            return result;
        },
        diff_rank: function (n) {
            var result = '';
            var pre = colorSpan({
                num: n,
                p_sign: '↓ ',
                m_sign: '↑ ',
                n_sign: ''
            });
            result = pre + Math.abs(n) + '</i>';
            return result;
        },
        commifySmall: function (n, isArrow) {
            var result = '';
            var pre = '';
            var after = '';
            if (n > 0) {
                if (isArrow) {
                    after = '↑';
                }
                pre = '<i style="font-style:normal;color:' + app.edition_color.up + ';text-align:right;">' + after;
            } else if (n < 0) {
                if (isArrow) {
                    after = '↓';
                }
                pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';text-align:right;">' + after;
            } else {
                pre = '<i style="font-style:normal;text-align:right;">';
            }
            if (n === 0) {
                result = '</i>';
            } else {
                result = pre + (Math.round(n * 100) / 100) + '</i>';
            }
            return result;
        },
        time: function (n) {
            if (n) {
                return moment(+n).format('YY.MM.DD HH:mm:ss');
            } else {
                return 'NA';
            }
        },
        time2: function (n) {
            if (n) {
                return moment(+new Date(n)).format('YY.MM.DD HH:mm:ss');
            } else {
                return 'NA';
            }
        },
        date: function (n) {
            return moment(n).format('YYYY.MM.DD');
        },
        date_from_db: function (n) {
            return moment(n, 'YYYYMMDD').format('YYYY.MM.DD');
        },
        time_from_db: function (n) {
            return moment(n, 'HHmmss').format('HH:mm:ss');
        },
        commify: function (n, end) {
            return commify(n, end);
        },
        commify2: function (n) {
            return commify(n, 2);
        },
        use_cut_price: function (val) {
            var result = '사용안함';
            if (val === 'Y') {
                result = '사용함';
            }
            return result;
        },
        expirationDate_date: function (date) {
            var dateText = ('' + date);
            if (dateText.length === 13) {
                return moment(+date).format('YYYY.MM.DD');
            } else {
                return '';
            }
        },
        stockname: function (val) {
            var result = val;
            if (app.stockcodeMap[val] && app.stockcodeMap[val].name) {
                result = app.stockcodeMap[val].name;
            }
            return result;
        },
        diff_regprice: function (value, obj) {
            var diff = obj.record.raw.price - value;
            return formatter.commifyColor(diff);
        },
        is_active: function (value) {
            var result = '';
            if (value === 'Y') {
                result = '<i style="font-style:normal;color:' + app.edition_color.up + ';text-align:right;">활성화</i>';
            } else if (value === 'N') {
                result = '<i style="font-style:normal;color:grey;text-align:right;">비활성</i>';
            }
            return result;
        },
        account_password: function (value, obj) {
            console.log(value);
            var account_number = obj.record.raw.account_number;
            var ta = app.common.utils.targetAccount(account_number);
            var result = '0000';
            if (ta) {
                result = ta.account_password;
            }
            return result;
        },
        converted_order_count: function (value, obj) {
            var qty = value || obj.record.raw.sell_qty;
            return formatter.commify(qty);
        },
        complete: function (val) {
            var pre = '';
            if (val === 'N') {
                pre = '<i style="font-style:normal;color:' + app.edition_color.down + ';">진행중</i>';
            } else {
                pre = '<i style="font-style:normal;">완료</i>';
            }
            return pre;
        },
        linebreak: function (val) {
            return val.split('\n').join('<br/>');
        },
        is_opend: function (val) {
            var result = '#FFF';
            var color_set = {
                '연동가능': '#FF9800',
                '완판': '#fc033d',
                '연동불가': '#ccc8c9'
            };
            if (color_set[val]) {
                result = color_set[val];
            }
            return '<i style="font-style:normal;color:' + result + ';">' + val + '</i>';
        }
    };
    app.common.formatter = formatter;
}());