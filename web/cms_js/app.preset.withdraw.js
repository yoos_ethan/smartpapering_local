/*global require, module, console, app, Ext, $, _, moment*/
(function () {
    'use strict';
    var fset = app.common.field_set;

    var reloadInfo = function(){
        var withdraw_store = Ext.getCmp('withdraw').getStore();
        var endWithdraw_store = Ext.getCmp('end_withdraw').getStore();

        withdraw_store.reload();
        endWithdraw_store.reload();
    }

   
    
    var editFn = function (ignore,context) { 
            var data = context.record.getData();
            delete data.rownum;

            var temp = {};
            
            _.each(_.keys(context.record.modified), function (key) {
                temp[key] = context.record.data[key];
            });

            Ext.Msg.confirm('수정 전 확인', `해당 출금정보를 ${context.value}로 수정하시겠습니까?`, function (res) {
                if (res === 'yes') {
                        if(temp){
                            var token = window.sessionStorage.getItem('login_info');
                            
                            if(token){
                                $.ajax({
                                    type: 'PUT',
                                    url: '/api/cms/withdrawupdate/',
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader("Authorization","Bearer " + token);
                                    },
                                    data: {
                                        substitute : JSON.stringify(temp),
                                        withdrawnum : data.withdrawnum
                                    },
                                    success: function () {
                                        app.combo.sendWithdrawPush(data, context.value);
                                        app.combo.sendWithdrawTele(data, context.value);
                                        context.record.commit();
                                        reloadInfo();
                                        Ext.Msg.alert('성공', "출금정보 업데이트를 완료했습니다.");
                                    },
                                    error: function (err) {
                                        console.log(err);
                                    }
                                });
                            }else{
                                Ext.Msg.alert('권한', '권한이 없습니다.');
                            }
                          
                        }else{
                            Ext.Msg.alert('실패', "출금정보 업데이트할 수 없습니다");
                        }
                }else{
                    context.record.store.reload()
                }
            });
    };

    var deleteUser = function () {
        return {
            xtype: 'button',
            text: '출금삭제',
            disabled: app.roleSettings.deleteUser(),
            handler: function () {
                var that = this;
                deleteAction(that);
            }
        };
    };

    var deleteAction = function (that) {
        var grid = that.up().up();
        var selections = grid.getView().getSelectionModel().getSelection();
        
        if (selections.length === 0) {
            Ext.Msg.alert('안내', '선택된 출금이 없습니다.');
            return false;
        }
        Ext.Msg.confirm('삭제 전 확인', '삭제 후 데이터 복구가 불가능합니다. 진행할까요?', function (res) {
            if (res === 'yes') {
                var seqs = [];
                _.each(selections, function (selection) {
                    seqs.push(selection.data.withdrawnum);
                });

                var token = window.sessionStorage.getItem('login_info');
                if(token){
                    $.ajax({
                        method: 'DELETE',
                        url: '/api/cms/withdrawdelete/'+seqs,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization","Bearer " + token);
                        },
                        success: function () {
                            grid.getStore().remove(selections);
                            reloadInfo();
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }else{
                    Ext.Msg.alert('권한', '권한이 없습니다.');
                }
             
            }
        });
    };

    app.preset.withdraw = function(){
        return {
            uid: 'withdraw',
            url: '/api/cms/withdraw/',
            is_sub_window: true,
            maximized: true,
            use_checkbox: false,
            use_search: true,
            use_paging : true,
            fields: [
                'rownum',
                'createdAt', //접수일
                'bank', // 출금은행
                'account_num', // 출금계좌
                'withdraw_price',//출금금액
                'account_holder', // 예금주
                'user_name', // 회원명
                'user_phone', // 회원전화번호
                'status', //출금상태
                'withdrawnum', //출금고유번호
                'user_id' //회원고유번호
            ],
    
            cols: [
                fset.rownum,
                fset.withdrawCreateDate, //접수일
                fset.bank, // 출금은행
                fset.accountNum, // 출금계좌
                fset.withdrawPrice, //출금금액
                fset.accountHolder, //예금주
                fset.userName, //회원명
                fset.userPhone, //회원전화번호
                fset.withdrawStatus, //출금상태
                fset.withdrawNum, //출금고유번호
                fset.userId //회원고유번호
            ],
            features: [],
            use_filter: true,
            additional_tbars:[deleteUser()],
            listeners:{
                edit : function(ignore, context){
                    editFn(ignore,context);
                }
            }
        }
      
    }
           

  
}());