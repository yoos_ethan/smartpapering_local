/*global require, module, app, console, window, Ext*/
(function () {
    'use strict';

    var app = window.app;

    var getWidth = function (opt) {
        var width = opt.width || opt.header.length * 12;
        if (opt.width === undefined && width < 80) {
            width = 80;
        }
        return width;
    };

    var field_maker = {
        basic: function (opt) {
            var temp = {
                header: opt.header,
                dataIndex: opt.dataIndex,
                width: getWidth(opt),
                align: opt.align || 'right',
                sortable: true
            };

            if (opt.flex) {
                delete temp.width;
                temp.flex = opt.flex;
            }
            if (opt.renderer) {
                temp.renderer = opt.renderer;
            }
            if (opt.textarea_editable) {
                // temp.header = '*' + temp.header;
                temp.editor = {
                    xtype: 'textarea',
                    allowBlank: false,
                    height: 100
                };
            } else if (opt.text_editable) {
                // temp.header = '*' + temp.header;
                temp.editor = {
                    xtype: 'textfield',
                    allowBlank: false
                };
            } else if (opt.number_editable) {
                // temp.header = '*' + temp.header;
                temp.editor = {
                    xtype: 'numberfield',
                    allowBlank: false
                };
            }
            if (opt.combo_editable_order){
                temp.editor = {
                    xtype : 'combo',
                    editable : false,
                    allowBlank : false,
                    displayField : 'name',
                    valueField : 'status',
                    store : Ext.create('Ext.data.Store', {
                        fields : ['name', 'status'],
                        storeId : 'order_status',
                        data : [
                            {name : '시공접수', status : '시공접수'},
                            {name : '시공진행', status : '시공진행'},
                            {name : '시공완료', status : '시공완료'},
                            {name : '거래취소', status : '거래취소'}                            
                        ]
                    })
                    
                }
            }
            if(opt.combo_editable_withdraw){
                temp.editor = {
                    xtype : 'combo',
                    editable : false,
                    allowBlank : false,
                    displayField : 'name',
                    valueField : 'status',
                    store : Ext.create('Ext.data.Store', {
                        fields : ['name', 'status'],
                        storeId : 'withdraw_status',
                        data : [
                            {name : '접수', status : '접수'},
                            {name : '완료', status : '완료'},                  
                        ]
                    })
                }
            }
            if (opt.nullable && temp.editor) {
                temp.editor.allowBlank = true;
            }
            if (opt.locked !== undefined) {
                temp.locked = opt.locked;
            }
            if (opt.disabled !== undefined) {
                temp.disabled = opt.disabled;
                if (opt.disabled === true) {
                    delete temp.editor;
                }

            }
            if (opt.summaryType) {
                temp.summaryType = opt.summaryType;
            }
            if (opt.hidden) {
                temp.hidden = true;
            }
            if (opt.tooltipField) {
                temp.tooltipField = opt.tooltipField;
            }

            return temp;
        },
        combobox: function (opt) {
            var column = {
                text: opt.header,
                dataIndex: opt.dataIndex,
                width: getWidth(opt),
                align: opt.align || 'right',
                sortable: true,
                editor: {
                    xtype: 'combobox',
                    displayField: 'name',
                    valueField: 'value',
                    disabled: opt.disabled || false,
                    editable: opt.editable || false,
                    store: Ext.create('Ext.data.Store', {
                        fields: ['value', 'name'],
                        data: app.combo[opt.comboKey]
                    })
                },
                renderer: opt.renderer || function (val) {
                    var res = '';
                    var obj = app.combo[opt.mapKey][val];
                    if (obj) {
                        res = obj.name;
                    }
                    return res;
                }
            };

            if (opt.locked !== undefined) {
                column.locked = opt.locked;
            }

            return column;
        }
    };
    app.common.field_maker = field_maker;
}());