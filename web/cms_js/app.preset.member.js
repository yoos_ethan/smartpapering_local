/*global require, module, console, app, Ext, $, _, moment*/
(function () {
    'use strict';
    var fset = app.common.field_set;

    var reloadInfo = function(){
        var user_store = Ext.getCmp('member').getStore();
        
        user_store.reload();
    }

   
    
    var editFn = function (ignore,context) { 
            var data = context.record.getData();
            delete data.rownum;

            var temp = {};
            _.each(_.keys(context.record.modified), function (key) {
                temp[key] = context.record.data[key];
            });

            Ext.Msg.confirm('수정 전 확인', '회원 데이터를 수정하시겠습니까?', function (res) {
                if (res === 'yes') {
                    if(temp){
                        var token = window.sessionStorage.getItem('login_info');
                        if(token){
                            $.ajax({
                                type: 'PUT',
                                url: '/api/cms/userupdate/',
                                data: {
                                    substitute : JSON.stringify(temp),
                                    unique : data.unique
                                },
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("Authorization","Bearer " + token);
                                },
                                success: function (res) {
                                    context.record.commit();
                                    reloadInfo();
                                    Ext.Msg.alert('성공', "회원정보 업데이트를 완료했습니다.");
                                },
                                error: function (err) {
                                    console.log(err);
                                }
                            });
                        }else{
                            Ext.Msg.alert('권한', '권한이 없습니다.');
                        }
                    }else{
                        Ext.Msg.alert('실패', "회원정보 업데이트할 수 없습니다");
                    }
                   
                }else{
                    context.record.store.reload()
                }
            });
    };

    var deleteUser = function () {
        return {
            xtype: 'button',
            text: '회원삭제',
            disabled: app.roleSettings.deleteUser(),
            handler: function () {
                var that = this;
                deleteAction(that);
            }
        };
    };

    var deleteAction = function (that) {
        var grid = that.up().up();
        var selections = grid.getView().getSelectionModel().getSelection();
        
        if (selections.length === 0) {
            Ext.Msg.alert('안내', '선택된 회원이 없습니다.');
            return false;
        }
        Ext.Msg.confirm('삭제 전 확인', '삭제 후 데이터 복구가 불가능합니다. 진행할까요?', function (res) {
            if (res === 'yes') {
                var seqs = [];
                _.each(selections, function (selection) {
                    seqs.push(selection.data.unique);
                });
                var token = window.sessionStorage.getItem('login_info');
                if(token){
                    $.ajax({
                        method: 'DELETE',
                        url: '/api/cms/userdelete/'+seqs,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization","Bearer " + token);
                        },
                        success: function () {
                            grid.getStore().remove(selections);
                            grid.getView().refresh();
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }else{
                    Ext.Msg.alert('권한', '권한이 없습니다.');
                }

            }
        });
    };

    app.preset.member = function(){
        return {
            uid: 'member',
            url: '/api/user/cmsusers/',
            is_sub_window: true,
            maximized: true,
            use_checkbox: false,
            use_search: true,
            fields: [
                'rownum',
                'createdAt', //계약일자
                'username', // 이름
                'phone', // 전화번호
                'point', // 포인트
                'unique', //회원고유번호
            ],
    
            cols: [
                fset.rownum,
                fset.createDate, //계약일자
                fset.memberName, // 회원명   
                fset.memberPhone,
                fset.point,
                fset.unique
            ],
            features: [],
            use_paging : true,
            use_filter: true,
            additional_tbars:[deleteUser()],
            listeners:{
                edit : function(ignore, context){
                    editFn(ignore,context);
                },
                afterrender : function(){
                    var member_store = Ext.getCmp('member').getStore();
                }
            }
        }
      
    }
           

  
}());