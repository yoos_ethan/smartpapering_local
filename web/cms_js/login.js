/*global require, module, console, $, window, app*/
$(function () {
    'use strict';
    var $userid = $('#userid');
    var $password = $('#password');

    $(document).ready(function(){
        var loginInfo = window.sessionStorage.getItem('login_info');
        if(loginInfo){
            window.location.href = '/cms/dashboard';
        }
    })

    $('.submit-action').click(function (e) {
        e.preventDefault();
        $.ajax({
            url : '/api/cms/login',
            type : 'POST',
            data : {
                admin_id : $userid.val(),
                password : $password.val()
            },
            success : function(res){
                if(!res.result){
                    window.alert('아이디 혹은 비밀번호가 올바르지 않습니다.');
                    window.sessionStorage.removeItem('login_info');
                    $userid.focus();
                }else{
                    window.location.href = '/cms/dashboard';
                    window.sessionStorage.setItem('login_info', res.token)
                }
            },
            error: function(err){
                window.alert('아이디 혹은 비밀번호를 확인해주세요.');
                var error = new Error(err);
                console.error(error);
            }
        })
    });
});