/*global Ext, window, APP, _, console, setTimeout, app, $*/
(function () {
    'use strict';
    var formPanel = function (useroption) {
        var formoption = {
            bodyPadding: 14,
            frame: true,
            flex: 1,
            buttonAlign: 'center',
            items: [],
            buttons: []
        };
        if (useroption) {
            formoption = _.extend(formoption, useroption);
        }
        var formWindow = Ext.create('Ext.form.Panel', formoption);
        return formWindow;
    };
    var common_panel = function (windowopt, formopt, usForm, onlyForm) {
        // console.log('windowopt : '+windowopt,'formopt : '+ formopt,'usform : '+ usForm,'onlyform : '+ onlyForm)/*global require, module, console, app, $, Ext, _, window*
        var formItems = [formPanel(formopt)];
        if (usForm === false) {
            formItems = null;
        }
        if (onlyForm) {
            return formItems;
        }
        var winOpt;
        winOpt = {
            width: 800,
            closable: false,
            border: false,
            layout: 'fit',
            items: formItems,
            maximizable: false,
            modal: false,
            constrain: false,
            header : false
        };
        if (windowopt) {
            winOpt = _.extend(winOpt, windowopt);
        }
        if (winOpt.id === undefined) {
            winOpt.id = 'rd_' + (+new Date());
        }
        // console.log(winOpt);
        var old = Ext.getCmp(winOpt.id);
        if (winOpt.pos && winOpt.pos.length === 4) {
            winOpt.x = winOpt.pos[0];
            winOpt.y = winOpt.pos[1];
            winOpt.width = winOpt.pos[2];
            winOpt.height = winOpt.pos[3];
        }
        if (old) {
            old.show();
        } else {
            if (winOpt.listeners === undefined) {
                app.MASK.show();
                winOpt.listeners = {
                    boxready: function () {
                        app.MASK.hide();
                    }
                };
            }
            var finalWin = Ext.create('Ext.window.Window', winOpt);
            var pg = winOpt.playground || 'playground';
            var playground = Ext.getCmp(pg);
            playground.add(finalWin);
            finalWin.show();
            // Ext.create('Ext.window.Window', winOpt).show();
        }
    };
    app.method.common_panel = common_panel;
}());