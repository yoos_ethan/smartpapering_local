/*global require, module, console, Ext, $, window, app, io, _, setTimeout*/
$(function () {
    'use strict';

    var createView = function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [{
                region: 'center',
                flex: 1,
                autoScroll: true,
                layout: 'fit',
                items: [{
                    xtype: 'container',
                    id: 'playground',
                    border: false,
                    layout: 'fit',
                    items: [],
                    listeners: {
                        boxready: function () {
                            app.roleSettings.start_view();
                        }
                    }
                }]
            }],
            listeners: {
                boxready: function () {
                    console.log('레이아웃 준비 완료');
                }
            }
        });
    };

    Ext.onReady(function () {
        var loginInfo = window.sessionStorage.getItem('login_info');
        if(!loginInfo){
            window.location.href = '/cms';
        }else{
            app.MASK = new Ext.LoadMask(Ext.getBody(), {
                msg: 'loading...'
            });
            app.MASK.show();
            createView();
        }
    });


});