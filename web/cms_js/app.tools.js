/*global app, require, module, console, Ext, moment, _*/
(function () {
    'use strict';
    var getFullRowStr = function (trec) {
        var fullstr = '';
        _.each(trec, function (val) {
            if (typeof val === 'string') {
                var sval = '' + val;
                fullstr += sval.toUpperCase() + '_';
            }
        });
        return fullstr;
    };
    var converter = function (obj) {
        var result = _.extend({}, obj);
        if (result.col35 !== undefined && app.combo.product_map[result.col35] !== undefined) {
            result.service_name = app.combo.product_map[result.col35].name;
        } else if (result.serviceId !== undefined && app.combo.product_map[result.serviceId] !== undefined) {
            result.service_name = app.combo.product_map[result.serviceId].name;
        }
        if (result.col03 && app.combo.partner_map[result.col03]) {
            result.partner_name = app.combo.partner_map[result.col03].name;
        }
        // console.log(result);
        return result;
    };
    app.tools.searchAlgo = function (obj) {
        var store = obj.up('panel').getStore();
        var searchStr = obj.lastValue;
        if (searchStr) {
            searchStr = searchStr.toUpperCase();
        }
        if (searchStr === '') {
            store.clearFilter();
            return false;
        }
        store.filterBy(function (rec) {
            var fullstr = getFullRowStr(converter(rec.raw));
            if (fullstr) {
                fullstr = fullstr.toUpperCase();
            }
            if (searchStr) {
                searchStr = searchStr.toUpperCase();
            }
            if (fullstr && fullstr.includes(searchStr)) {
                return true;
            } else {
                return false;
            }
        });
    };

    var myGridReload = function (mygrid, obj) {
        var store = mygrid.getStore();
        if (obj.use_paging) {
            Ext.apply(store, {
                pageSize: app.userData.pageSize
            });
            store.setProxy({
                type: 'ajax',
                timeout: 120000,
                api: {
                    read: obj.url
                },
                reader: {
                    root: 'items',
                    totalProperty: 'count'
                }
            });
        } else {
            store.setProxy({
                pageParam: undefined,
                startParam: undefined,
                limitParam: undefined,
                type: 'ajax',
                timeout: 120000,
                api: {
                    read: obj.url
                },
                reader: {
                    root: 'items'
                }
            });
        }
        store.reload();
    };


    app.tools.proxyUpdate = function (obj, grid) {
        if (Ext.getCmp(obj.uid) === undefined) {
            return false;
        }
        var mygrid = null;
        if (grid === undefined && Ext.getCmp(obj.uid)) {
            mygrid = Ext.getCmp(obj.uid).down('panel');
            if (mygrid === null && obj.uid.includes('tab')) {
                mygrid = Ext.getCmp(obj.uid);
            }
        } else {
            mygrid = grid;
        }
        // var url = obj.url + app.tools.urlExpand(obj);
        if (mygrid) {
            myGridReload(mygrid, obj);
        }
    };
    var comboMap = {};
    var filterProxy = function (that, query, default_value) {
        var win = that.up('window');
        var grid = win.down('grid');
        var oldStore = grid.getStore();
        var old_url = oldStore.getProxy().api.read;
        var pid = that.up('window').down('grid').id;
        old_url = old_url.split('?')[0];
        comboMap[pid][query] = that.value;
        var myComboMap = comboMap[pid];
        var url = [];
        _.each(myComboMap, function (val, key) {
            if (val !== default_value) {
                url.push(key + '=' + val);
            }
            oldStore.proxy.api.read = old_url + '?' + url.join('&');
        });
        oldStore.clearData();
        oldStore.load();
    };

    // app.tools.proxyCombo({
    //     title : '리모트',
    //     name : 'remote_filter',
    //     default_data : app.combo.remote,
    //     default_value: '전체',
    //     labelWidth : 40,
    //     width : 120,
    //     query : 'filter'
    // });

    app.tools.proxyCombo = function (opt) {
        var title = opt.title;
        var name = opt.name;
        var default_data = opt.default_data;
        var default_value = opt.default_value;
        var labelWidth = opt.labelWidth || 40;
        var width = opt.width || 120;
        var query = opt.query;
        // parent_uid: 'customer_summary'
        if (comboMap[opt.parent_uid] === undefined) {
            comboMap[opt.parent_uid] = {};
        }
        comboMap[opt.parent_uid][opt.query] = opt.default_value;
        
        var box = _.extend([], default_data);
        box.unshift({
            name: default_value,
            value: default_value
        });
        return app.combo.comboTem({
            title: title,
            name: name,
            default_data: box,
            default_value: default_value
        }, {
            labelWidth: labelWidth,
            width: width,
            listeners: {
                change: function () {
                    var that = this;
                    filterProxy(that, query, default_value);
                }
            }
        });
    };

    app.tools.date_picker = function (nameStr, uid) {
        var monFormat = 'Y.m.d';
        var yymmdd = 'YYYYMMDD';
        var xtype = 'datefield';

        var form = {
            xtype: xtype,
            format: monFormat,
            emptyText: nameStr,
            fieldLabel: nameStr,
            labelWidth: 40,
            width: 140,
            id: uid,
            labelSeparator: '',
            listeners: {
                blur: function () {
                    var that = this;
                    var sdate = moment(that.lastValue).format(yymmdd);
                }
            }
        };
        return form;
    };

}());