/*global require, module, console, app*/
(function () {
    'use strict';
    // 증권사 role030
    // 법인 role020
    var is_security = function () {
        return app.session.userrole === 'role030';
    };
    var is_company = function () {
        return app.session.userrole === 'role020';
    };
    var is_basecamp = function () {
        return app.session.userrole === 'role090';
    };
    var roleSettings = {
        col_color_security: function () {
            var style = 'color:#5aff61;';
            return style;
        },
        col_color_company: function () {
            var style = 'color:#ffc71f;';
            return style;
        },
        col_color_remote: function () {
            var style = 'color:#eb75ff;';
            return style;
        },
        start_view: function () {
            app.window.dashboard();
        },
        status_disabled: function () { //원격담당
            var disabled = false;
            if (is_company()) {
                disabled = true;
            }
            if (is_security()) {
                disabled = true;
            }
            return disabled;
        },
        col_user_info: function () {
            var hidden = false;
            if (is_security()) {
                hidden = true;
            }
            return hidden;
        },
        col01_disabled: function () { //원격담당
            var disabled = false;
            if (is_security() || is_company()) {
                disabled = true;
            }
            return disabled;
        },
        col02_disabled: function () { //계약일자
            var disabled = false;
            if (is_security()) {
                disabled = true;
            }
            return disabled;
        },
        col03_disabled: function () { //계약일자
            var disabled = false;
            if (is_company()) {
                disabled = true;
            }
            return disabled;
        },
        col04_disabled: function () { //계약일자
            var disabled = false;
            if (is_security()) {
                disabled = true;
            }
            return disabled;
        },
        col05_disabled: function () { //연락처
            var disabled = false;
            if (is_security()) {
                disabled = true;
            }
            return disabled;
        },
        col14_disabled: function () { //증권사
            var disabled = false;
            if (is_security()) {
                disabled = true;
            }
            return disabled;
        },
        col15_disabled: function () { //계좌개설
            var disabled = false;
            if (is_company()) {
                disabled = true;
            }
            return disabled;
        },
        col16_disabled: function () { //관리자
            var disabled = false;
            if (is_company()) {
                disabled = true;
            }
            return disabled;
        },
        col17_disabled: function () { //수수료
            var disabled = false;
            if (is_company()) {
                disabled = true;
            }
            return disabled;
        },
        col19_disabled: function () { //수수료
            var disabled = false;
            if (is_company()) {
                disabled = true;
            }
            return disabled;
        },
        col18_disabled: function () { //원격일정
            var disabled = false;
            if (is_company() || is_security()) {
                disabled = true;
            }
            return disabled;
        },
        gridControl: function () {
            var hidden = false;
            if (is_company() || is_security()) {
                hidden = true;
            }
            return hidden;
        },
        settle: function () {
            var hidden = false;
            if (is_security()) {
                hidden = true;
            }
            return hidden;
        },
        menu_account: function () {
            var hidden = false;
            if (is_security()) {
                hidden = true;
            }
            return hidden;
        },
        is_basecamp: function () {
            var result = true;
            if (is_company() || is_security()) {
                result = false;
            }
            return result;
        },
        is_not_basecamp: function () {
            var result = false;
            if (is_company() || is_security()) {
                result = true;
            }
            return result;
        },
        is_company: function () {
            return is_company();
        },
        addUser: function () { //회원추가
            var disabled = false;
            if (is_security()) {
                disabled = true;
            }
            return disabled;
        },
        deleteUser: function () { //회원삭제
            var disabled = false;
            if (is_security()) {
                disabled = true;
            }
            return disabled;
        },
        company_settle: function () {
            var result = '';
            if (is_basecamp()) {
                result = 'is_basecamp';
            } else if (is_company()) {
                result = 'is_company';
            } else if (is_security()) {
                result = 'is_security';
            }
            return result;
        }
    };
    app.roleSettings = roleSettings;
}());