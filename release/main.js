/*global require, module, console, process*/
(function () {
    'use strict';
    var electron = require('electron');
    var electronApp = electron.app;
    var BrowserWindow = electron.BrowserWindow;
    var mainWindow;
    var createWindow;
    createWindow = function () {
        mainWindow = new BrowserWindow({
            width: 1400,
            height: 812,
            center: true,
            webPreferences: {
                nodeIntegration: false
            }
        });
        mainWindow.loadURL('https://smartpapering.com/cms'); //항상 이렇게 맞추둘것
        mainWindow.on('close', function () {
            mainWindow = null;
        });
        mainWindow.on('closed', function () {
            mainWindow = null;
        });
        mainWindow.setMenuBarVisibility(false);
    };

    electronApp.on('ready', createWindow);
    // electronApp.releaseSingleInstanceLock();
    // Quit when all windows are closed.
    electronApp.on('window-all-closed', function () {
        if (process.platform !== 'darwin') {
            electronApp.quit();
        }
    });
    electronApp.on('activate', function () {
        if (mainWindow === null) {
            createWindow();
        }
    });
}());